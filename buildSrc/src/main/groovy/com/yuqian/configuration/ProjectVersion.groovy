package com.yuqian.configuration

class ProjectVersion {
    Integer major
    Integer minor
    Boolean release
    ProjectVersion(Integer major, Integer minor) {
        this.major = major
        this.minor = minor
    }
    ProjectVersion(Integer major, Integer minor, Boolean release) {
        this(major, minor)
        this.release = release
    }

    @Override
    public String toString() {
        return "$major.$minor${release ? '' : '-SNAPSHOT'}";
    }
}